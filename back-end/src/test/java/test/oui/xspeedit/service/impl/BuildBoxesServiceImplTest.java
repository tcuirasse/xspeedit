/**
 * 
 */
package test.oui.xspeedit.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import test.oui.xspeedit.XspeedItApp;

/**
 * @author tmhemazro
 * Test class for the AccountResource REST controller.
 *
 * @see BuildBoxesServiceImpl
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = XspeedItApp.class)
public class BuildBoxesServiceImplTest {

	@Autowired
	private BuildBoxesServiceImpl buildBoxesServiceImpl;
	
	/**
	 * Test method for {@link test.oui.xspeedit.service.impl.BuildBoxesServiceImpl#buildBoxes(java.lang.String)}.
	 */
	@Test
	public void testBuildBoxes() {
		
		String chain1 = "12548796587968"; 
		String chain2 = "163841689525773";   //163841689525773
		
		int sum =0; 
		int nbMinBoxes = 0;
		for (int i = 0; i < chain1.length(); i++) {
			sum+= Character.getNumericValue(chain1.charAt(i));
		}
		nbMinBoxes = (int) Math.ceil(sum /10.0); 
		List<String>  boxes= buildBoxesServiceImpl.buildBoxes(chain1);
		assertEquals(true, boxes.size() >= nbMinBoxes);
		sum = 0;
		for (int i = 0; i < chain2.length(); i++) {
			sum+= Character.getNumericValue(chain2.charAt(i));
		}
		nbMinBoxes = (int) Math.ceil(sum /10.0); 
		assertEquals(nbMinBoxes, buildBoxesServiceImpl.buildBoxes(chain2).size());
	}

	/**
	 * Test method for {@link test.oui.xspeedit.service.impl.BuildBoxesServiceImpl#foundLowest(int[], int)}.
	 */
	@Test
	public void testFoundLowest() {
		{
			int chars[] = {0,0,0,0};
			assertEquals(buildBoxesServiceImpl.foundLowest(chars, 0), -1);
			assertEquals(buildBoxesServiceImpl.foundLowest(chars, 10), -1);
		}
		{
			int chars[]={0,1,1,0,0,0,1,1,1};
			assertEquals(buildBoxesServiceImpl.foundLowest(chars, 0), 1);
			assertEquals(buildBoxesServiceImpl.foundLowest(chars, 8), 1);
			assertEquals(buildBoxesServiceImpl.foundLowest(chars, 9), -1);
		}
	}

	/**
	 * Test method for {@link test.oui.xspeedit.service.impl.BuildBoxesServiceImpl#foundGreatest(int[], int)}.
	 */
	@Test
	public void testFoundGreatest() {
		
		{
			int chars[] = {0,0,0,0};
			assertEquals(buildBoxesServiceImpl.foundGreatest(chars, 0), -1);
			assertEquals(buildBoxesServiceImpl.foundGreatest(chars, 9), -1);
		}
		{
			int chars[]={0,1,1,0,0,0,1,1,1};
			assertEquals(buildBoxesServiceImpl.foundGreatest(chars, 1), 8);
			assertEquals(buildBoxesServiceImpl.foundGreatest(chars, 0), 8);
			assertEquals(buildBoxesServiceImpl.foundGreatest(chars, 5), 2);
		}
	}

}
