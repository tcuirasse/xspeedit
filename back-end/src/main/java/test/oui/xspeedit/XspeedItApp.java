package test.oui.xspeedit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class XspeedItApp {
	
	public static void main(String[] args) {
		
		SpringApplication.run(XspeedItApp.class, args);
	}
}
