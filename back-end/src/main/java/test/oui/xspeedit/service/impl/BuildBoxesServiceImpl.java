/**
 * 
 */
package test.oui.xspeedit.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import test.oui.xspeedit.service.BuildBoxesService;


/**
 * Service Implementation for building boxes.
 * @author tmhemazro
 */
@Service
public class BuildBoxesServiceImpl implements BuildBoxesService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BuildBoxesServiceImpl.class);

	/**
	 * @see test.oui.xspeedit.service.impl.BuildBoxesService#buildBoxes(java.lang.String)
	 */
	public List<String> buildBoxes(String chain) {
		ArrayList<String> boxes = new ArrayList<String>();
		
		if (!StringUtils.isEmpty(chain) && chain.matches("[1-9]+")) {
			
			// build table of occurence between 1-9 in the chain
	        int[] itemsTable = new int[9];
	        for (char c : chain.toCharArray()) {
	        	itemsTable[c-'1'] ++;
	        }
	        while(true) {
	            String box = "";
	            int value = 0;
	            while (value <= 10) {
	                int l = foundLowest(itemsTable, value);
	                if (l != -1) {
	                    value += l + 1;
	                    itemsTable[l] --;
	                    box += (char)('1' + l);
	                }
	    
	                int g = foundGreatest(itemsTable, value);
	                if (g != -1) {
	                    value += g + 1;
	                    itemsTable[g] --;
	                    box += (char)('1' + g);
	                }
	                if (l == -1 && g == -1) {
	                    break;
	                }
	            }
	            if (value > 0) {
	                boxes.add(box);
	            } else {
	                break;
	            }
	        }
        	return boxes;
		}
		else {
			LOGGER.error("The string must contain only numerics {} ", chain);
			throw new IllegalArgumentException("The string must contain only numerics "+ chain);
		}
	}
	
	/**
	 * Find from the table the lowest value that can be add to the boxe
	 * @param chars
	 * 		table of occurence between 1-9 in the chain.
	 * @param v
	 * 		current value of the boxe
	 * @return
	 * 		the lowest value found in the table
	 * 		-1 if nor found
	 */
	protected int foundLowest(int[] chars, int v) {
        for (int i = 0; i < chars.length; i++ ) {
            if (chars[i] > 0) {
                if (i + 1 + v <= 10) {
                    return i;
                }
            }
        }
        return -1;
    }

	/**
	 * 
	 * Find from the table the greatest value that can be add to the boxe
	 * @param chars
	 * 		table of occurence between 1-9 in the chain.
	 * @param v
	 * 		current value of the boxe
	 * @return
	 * 		the lowest value found in the table
	 * 		-1 if nor found
	 */
    protected int foundGreatest(int[] chars, int v) {
        for (int i = chars.length-1; i >= 0; i-- ) {
            if (chars[i] > 0) {
                if (i + 1 + v <= 10) {
                    return i;
                }
            }
        }
        return -1;
    }
}
