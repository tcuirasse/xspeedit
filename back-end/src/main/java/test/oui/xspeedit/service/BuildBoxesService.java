package test.oui.xspeedit.service;

import java.util.List;

/**
 * Service Interface for building boxes.
 * @author tmhemazro
 */
public interface BuildBoxesService {

	/**
	 * Build a list of string of optimised boxes. Each string in the list represent one boxe
	 * and a boxe not exceed a weight of 10. 
	 * @param chain
	 * 		chain of items represented by a string 
	 * @return
	 * 		list of string of optimized boxes
	 */
	List<String> buildBoxes(String chain);
}
